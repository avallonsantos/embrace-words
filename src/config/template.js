const path = require('path')
// Importing meta tags for this landing page
const Metas = require('./../config/metas.js')

// Exporting literal object with configuration of HTML Webpack Plugin

module.exports = {
    links: [],
    title: 'Palavras que Abraçam | ClickBus',
    mobile: true,
    lang: 'pt_BR',
    template: path.resolve(__dirname, './../components/index.html'),
    meta: Metas,
    renderPlaceholders: false
}