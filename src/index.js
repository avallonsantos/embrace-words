import 'regenerator-runtime/runtime';

import { v4 as uuid } from 'uuid';
import Pristine from 'pristinejs';
import {
    successModal,
    findCorrectRegionByState,
    findBlockedMessages,
    toggleModalVisibility,
    displayMessagesByRegion
} from './utils';

// Importing shared scss file
import './assets/scss/shared.scss';

import firebase from 'firebase/app';
import 'firebase/firestore'

const landingPageContent = document.getElementById('landing-page-content');

const runLandingPageApplication = () => {
    // Constants
    const messageListWrapper = document.querySelector('.ew-current-messages-list-wrapper ul');
    const blockedWords = [
        'lixo',
        'bosta',
        'horrível',
        'horroroso',
        'nojento',
        'podre',
        'buser',
        'mentiroso',
        'abandonado',
        'abandono',
        'parental',
        'alienação',
        'negativo',
        'Thammy',
        'Thamy'
    ];
    const statesByRegion = {
        norte: ['AC', 'AP', 'AM', 'PA', 'RO', 'RR', 'TO'],
        nordeste: ['AL', 'BA', 'CE', 'MA', 'PB', 'PE', 'PI','RN', 'SE'],
        centroOeste: ['GO', 'MT', 'MS'],
        sul: ['PR', 'RS', 'SC'],
        sudeste: ['ES', 'MG', 'RJ', 'SP']
    }

    /**
     * Firebase configuration
     * @type {{storageBucket: string, apiKey: string, messagingSenderId: string, appId: string, projectId: string, measurementId: string, databaseURL: string, authDomain: string}}
     */
    const firebaseConfig = {
        apiKey: "AIzaSyB-BFE9kxuUl0k0Wg1znUjC9AkaIWPG8K4",
        authDomain: "embrace-words.firebaseapp.com",
        databaseURL: "https://embrace-words.firebaseio.com",
        projectId: "embrace-words",
        storageBucket: "embrace-words.appspot.com",
        messagingSenderId: "561896439410",
        appId: "1:561896439410:web:e93908bbae0267490f4ceb",
        measurementId: "G-FWG9VVV5YD"
    };
    firebase.initializeApp(firebaseConfig);

    // Get and display messages
    const brazilMapLocations = document.querySelectorAll('.brazil-region');
    const currentRegionTitle = document.querySelector('.message-list-region');
    brazilMapLocations.forEach(region => {
        region.addEventListener('click', () => {
            const regionName = region.getAttribute('id');
            const formattedRegionName = region.getAttribute('data-name');
            const activeLocation = document.querySelector('.active-location');
            if(activeLocation) {
                activeLocation.classList.remove('active-location');
            }
            region.classList.add('active-location');
            currentRegionTitle.innerHTML = formattedRegionName;
            displayMessagesByRegion(regionName);
        })

    });


    /**
     * Get valid messages
     * @return {Promise<void>}
     */
    const getMessages = async () => {
        const database = firebase.firestore();
        const messagesAsFirebaseFormat = await database.collection('messages').get();
        const allMessages = messagesAsFirebaseFormat.docs.map((doc) => ({
            id: doc.id,
            ...doc.data(),
        }));
        const filteredMessages = allMessages.filter(message => message.isActive);
        filteredMessages.forEach(message => {
            const messageItem = `<li data-region="${message.region}"><strong>${message.name}</strong><p>${message.message}</p></li>`;
            messageListWrapper.insertAdjacentHTML('beforeend', messageItem);
        });
    }
    getMessages();


    /**
     * Send new message
     * @param formData
     * @return {Promise<void>}
     */
    const sendNewMessage = async (formData) => {
        const database = firebase.firestore();
        const uniqueID = uuid();
        try {
            await database.collection('messages').doc(uniqueID).set({ ...formData });
            toggleModalVisibility(true);
        } catch (error) {
            alert('Houve um erro ao enviar sua mensagem. Por favor, recarrege a página e tente novamente.');
            console.log(error);
        }
    }

    const EWForm = document.querySelector('#participation-form form');
    const formValidator = new Pristine(EWForm);
    EWForm.addEventListener('submit', async (e) => {
        e.preventDefault();

        const isFormValid = formValidator.validate();

        if(!isFormValid) return;

        const submitButton = document.getElementById('ew-send-new-message');
        submitButton.innerText = 'Enviando...';

        const EWInputs = EWForm.elements;
        const name = EWInputs[0].value;
        const state = EWInputs[1].value;
        const message = EWInputs[2].value;
        const messageHasBlockedMessage = findBlockedMessages(message, blockedWords);
        const isActive = !messageHasBlockedMessage;
        const region = findCorrectRegionByState(state, statesByRegion);

        const formData = {
            name,
            state,
            message,
            region,
            isActive
        }
        await sendNewMessage(formData);
        submitButton.innerText = 'Enviar';
        EWForm.reset();
    });

    /**
     * Close Modal triggers
     */
    const closeModal = document.getElementById('ew-close');

    const closeTriggers = [closeModal];

    closeTriggers.forEach(closeTrigger => {
        closeTrigger.addEventListener('click', () => {
            toggleModalVisibility(false)
        })
    })
};


if(!landingPageContent) {
    throw new Error('The page\'s main wrapper could not be found. Please wrap the entire application within an element with id "#landing-page-content". So we guarantee that we will not have any kind of conflict.')
}

runLandingPageApplication();