import PerfectScrollbar from 'perfect-scrollbar';
const messageListWrapper = document.querySelector('.ew-current-messages-list-wrapper ul');

/**
 * Find blocked words
 * @param text
 * @param blockedWords
 */
export const findBlockedMessages = (text, blockedWords) => {
    let hasBlockedMessage = false;
    blockedWords.forEach(blockedWord => {
        const findWord = text.search(blockedWord);
        if(findWord >= 0) {
            hasBlockedMessage = true;
        }
    });
    return hasBlockedMessage;
}

/**
 * Get state region
 * @param state
 * @param statesByRegion
 * @return {*}
 */
export const findCorrectRegionByState = (state, statesByRegion) => {
    let region;
    Object.keys(statesByRegion).forEach(regionList => {
        if(statesByRegion[regionList].includes(state)) {
            region = regionList;
        }
    });
    return region;
}

/**
 * Based on passed state, we show or hide the modal
 * @param showOrHide
 */

export const successModal = document.getElementById('ew-modal-success-overlay');

export const toggleModalVisibility = (showOrHide) => {
    if(showOrHide) {
        successModal.classList.add('success');
        return;
    }
    successModal.classList.remove('success');
}

// Custom Scrollbar
const messageWrapper = '.ew-current-messages-list-wrapper ul';

const ps = new PerfectScrollbar(messageListWrapper);

/**
 * Display messages by region
 * @param region
 */
export const displayMessagesByRegion = (region) => {
    const messages = document.querySelectorAll('.ew-current-messages-list-wrapper ul li');
    const fallbackMessage = document.querySelector('.ew-fallback-message');
    const emptyMessage = document.querySelector('.ew-fallback-empty');
    if(fallbackMessage) {
        fallbackMessage.style.display = 'none';
    }
    messages.forEach(message => {
        const messageRegion = message.getAttribute('data-region');
        if(messageRegion && messageRegion === region) {
            message.classList.add('active-message');
        } else {
            message.classList.remove('active-message');
        }
    });

    const activeMessages = document.querySelector('.ew-current-messages-list-wrapper ul li.active-message');

    if(!activeMessages) {
        emptyMessage.style.display = 'block';
    } else {
        emptyMessage.style.display = 'none';
    }

    ps.update();
    document.querySelector('.ew-current-messages-list-wrapper ul').scrollTop = 0;

}