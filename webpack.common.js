const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const Metas = require('./src/config/metas.js')
const htmlTemplate = require('./src/config/template.js')

// Initialization of Webpack config

module.exports = {
    entry: './src/index.js',
    output: {
        filename: 'lp.bundle.js',
        path: path.resolve(__dirname, './dist'),
        publicPath: ''
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel-loader',
                options: {
                    presets: [
                        '@babel/typescript',
                        '@babel/preset-env'
                    ],
                    plugins: ['@babel/plugin-proposal-class-properties']
                }
            },
            {
                test: /\.scss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(png|jpg?e|svg|gif)$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'img/'
                        }
                    }
                ]
            }
        ]
    },
    plugins:  [
        new MiniCssExtractPlugin({
            filename: 'lp.bundle.css',
            meta: {
                robots: 'index, follow'
            }
        }),
    ]
}
