const merge = require('webpack-merge')
const common = require('./webpack.common.js')
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

const htmlTemplate = require('./src/config/template.js')

htmlTemplate.renderPlaceholders = true;

const PROD_S3_LANDING_PAGE_URL = 'https://static.clickbus.com.br/landing-pages/';

module.exports = merge(common, {
    mode: 'production',
    output: {
        publicPath: PROD_S3_LANDING_PAGE_URL
    },
   
    plugins: [
        new HtmlWebpackPlugin({
            ...htmlTemplate,
            minify: false
        })
    ]
})