const path = require('path')
const merge = require('webpack-merge')
const common = require('./webpack.common.js')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackPartialsPlugin = require('html-webpack-partials-plugin')
const htmlTemplate = require('./src/config/template.js')

htmlTemplate.links.push({
    rel: 'stylesheet',
    href: 'https://www.clickbus.com.br/static/css/brazil_main.min.css?v=1.0-621'
})

module.exports = merge(common, {
    mode: 'development',
    optimization: {
        minimize: false
    },
    devtool: 'inline-source-map',
    devServer: {
        contentBase: './dist',
        index: 'index.html',
        compress: false,
        port: 9000
    },
    plugins: [
        new HtmlWebpackPlugin(htmlTemplate),
        new HtmlWebpackPartialsPlugin({
            path: './src/components/development-sections/app.header.html',
            priority: 'high'
        }),
        new HtmlWebpackPartialsPlugin({
            path: './src/components/development-sections/app.footer.html',
            priority: 'low'
        })
    ]
})
