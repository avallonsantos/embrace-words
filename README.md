### Guia rápido de instalação

Essa landing page foi desenvolvida com Webpack e os scripts de desenvolvimento e build de produção são, respectivamente, `yarn dev` e `yarn build`.

### Instalação

Depois de clonar esse repositório, por favor rode o comando:

```
yarn install
```
ou, se estiver usando npm:

```
npm install
```

para instalar todas as dependências do projeto.

### Configuração

Dentro da pasta **src/config** você encontrará dois arquivos que representam, respectivamente, as *meta tags* e as configurações do *HTML Template*. Essa versão que está trabalhando já está devidamente configurada com as palavras-chave da landing page **Palavras que Abraçam". No entanto, caso seja necessário adicionar, remover ou editar alguma propriedade, sinta-se à vontade.

#### Metas

O arquivo de [metas.js](src/config/metas.js) é composto por um array de objetos em que cada objeto representa um único meta dentro do site. Atualmente estão configurados os metas de **og:url** e **description**.

### Template

O arquivo de [template.js](src/config/template.js) é composto por um objeto que possui configurações pontuais sobre a estrutura do **Head do HTML**. Por exemplo, o **título**. 

> Toda modificação feita nesses arquivos requer uma atualização no servidor. Isto é, caso esteja com um servidor de desenvolvimento rodando, será necessário pará-lo e rodar novamente para que as modificações feitas nesse arquivo sejam refletidas na aplicação.

### Public Path

> Essa é provavelmente a configuração mais importante a ser feita dentro dos arquivos da aplicação.

Dentro do arquivo [webpack.prod.js](webpack.prod.js), você encontará as informações de build para produção da aplicação. Antes de criar uma versão de desenvolvimento, é necessário atualizar a constante `PROD_S3_LANDING_PAGE_URL` e apontar para a **URL correta do S3**.

**Como isso funciona**?

Como toda landing page, precisamos disponibilizar os assets (*CSS, scripts e imagens*) da aplicação em algum storage disponível e, na ClickBus, o S3 é usado para isso. Em geral, é criado uma nova pasta dentro de algum bucket e todos os arquivos são enviados para lá. Esse processo ainda precisa ser feito. Então, por favor, siga esses passos para configurar o Webpack para apontar para essa URL quando buildar a aplicação.

- Se necessário, atualize os arquivos de [metas.js](src/config/metas.js) e/ou [template.js](src/config/template.js) como descrito acima.
- Defina qual será a pasta do S3 que será utilizada para publicar os assets dessa aplicação. Note que, atualmente, dentro do [webpack.prod.js](webpack.prod.js), a constante `PROD_S3_LANDING_PAGE_URL` está apontando para um recurso genérico `https://static.clickbus.com.br/landing-pages/`. Vamos supor que decidam publicar os assets dessa aplicação dentro de uma pasta chamada `https://static.clickbus.com.br/landing-pages/dia-dos-pais-2020`. Então essa URL deve ser adicionada à constante `PROD_S3_LANDING_PAGE_URL` **antes de buildar a aplicação**.
- Uma vez atualizando essa constante, pode buildar a aplicação com `yarn build` ou `npm build`. O Webpack atualizará todos os caminhos dos assets (imagens, CSS, scripts) para a URL requerida.
- Depois de buildar aplicação, o último passo é enviar os assets para a pasta do bucket criado.

### Enviando os assets

- Com o build feito, uma nova pasta chamada `dist` será criada.
- Dentro dessa pasta estará tudo que precisa para puyblicar a landing page em produção. Tanto o `index.html` quanto os assets necessários para serem enviados ao S3.
- O processo é simples: pegue tudo o que **não for** o `index.html` e jogue dentro da pasta do S3 configurada na `PROD_S3_LANDING_PAGE_URL`. Não é recomendado mudar a estrutura de pasta antes de enviar para o S3. Ou seja, o `lp.bundle.css` e o `lp.bundle.js` devem ficar na raíz da pasta e as imagens devem ser enviadas dentro da pata `img`, exatamente como está dentro da pasta `dist`.
- Pronto, seu `index.html` estará corretamente apontando para o S3 e puxará os arquivos automaticamente.

### Observações

O processo acima configura corretamente o caminho de todos as **imagens, CSS e scripts** necessários para o funcionamento da aplicação. Entretanto, para essa landing page em específico, existe só uma última mudança que deve ser feita antes de buildar a aplicação.

- Depois que o usuário envia uma nova mensagem, um modal de sucesso é aberto. Dentro desse modal, existe uma imagem que precisa ser baixada pelo usuário caso ele queira. Esse download é propriciado através do atributo `download` especificado nas tags do tipo `anchor` **(a)**. Essa imagem [share-image.png](src/assets/public/img/share-image.png) deve ser manualmente enviada à pasta do S3. Essa URL pública deve ser atualizada dentro do arquivo [app.component.html](src/components/app/app.component.html) **na linha 148**. Após isso, poderá fazer o build sem problemas. Caso queira, pode editar manualmente o `index.html` gerado pelo build depois sem problemas algum.

### Desenvolvimento

Todos os arquivos de desenvolvimento ficam concentrados dentro da pasta [src](src). Todo HTML dessa aplicação fica dentro de [app.component.html](src/components/app/app.component.html). Os scripts da aplicação estão principalmente no arquivo principal [index.js](src/index.js). Os assets de *scss* estão dentro da pasta [src/assets/scss](src/assets/scss) e as imagens dentro da pasta [src/assets/public/img](src/assets/public/img). Para subir o servidor de desenvolvimento, rode o seguinte comando na raíz do seu projeto:

```
yarn dev
```

Ou, se estiver usando **npm**:

```
npm run dev
```

Um servidor será instanciado na url: `http://localhost:9000`.

### Build

Para buildar a aplicação, simplesmente rode na raiz do projeto:

```
yarn build
```

Ou, se estiver usando **npm**:

```
npm run build
```

### Conteúdo faltante

Até o momento de encerramento dessa LP, o conteúdo da última seção "Mensagem sobre a nossa campanha" não estava pronto, então será necessário editá-la antes de publicar. Sinta-se à vontade para editar ou, caso queira, pode enviar que eu edito sem problema algum.

Qualquer dúvida, estou à disposição.